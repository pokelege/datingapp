﻿namespace Matcher.Models
{
	public class MatchingModel
	{
		public User CurrentUser { get; set; }
		public User MatchingUser { get; set; }
	}
}
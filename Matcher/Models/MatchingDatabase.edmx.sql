
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/13/2015 14:33:11
-- Generated from EDMX file: C:\Users\Andrew.Huertas\Documents\datingapp\Matcher\Models\MatchingDatabase.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Database1];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_Matches_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Matches] DROP CONSTRAINT [FK_Matches_User];
GO
IF OBJECT_ID(N'[dbo].[FK_Matches_UserLike]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Matches] DROP CONSTRAINT [FK_Matches_UserLike];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Matches]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Matches];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Matches'
CREATE TABLE [dbo].[Matches] (
    [MatchId] int IDENTITY(1,1) NOT NULL,
    [UserId] int  NOT NULL,
    [UserLikeId] int  NOT NULL,
    [Liked] bit  NOT NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UserName] nvarchar(max)  NOT NULL,
    [Birthdate] datetime  NOT NULL,
    [IsMale] bit  NOT NULL,
    [Image] nvarchar(max)  NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [MatchId] in table 'Matches'
ALTER TABLE [dbo].[Matches]
ADD CONSTRAINT [PK_Matches]
    PRIMARY KEY CLUSTERED ([MatchId] ASC);
GO

-- Creating primary key on [Id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [UserId] in table 'Matches'
ALTER TABLE [dbo].[Matches]
ADD CONSTRAINT [FK_Matches_User]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Matches_User'
CREATE INDEX [IX_FK_Matches_User]
ON [dbo].[Matches]
    ([UserId]);
GO

-- Creating foreign key on [UserLikeId] in table 'Matches'
ALTER TABLE [dbo].[Matches]
ADD CONSTRAINT [FK_Matches_UserLike]
    FOREIGN KEY ([UserLikeId])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Matches_UserLike'
CREATE INDEX [IX_FK_Matches_UserLike]
ON [dbo].[Matches]
    ([UserLikeId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------
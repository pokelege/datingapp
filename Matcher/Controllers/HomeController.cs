﻿using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Matcher.Models;

namespace Matcher.Controllers
{
	public class HomeController : Controller
	{
		/// <summary>
		/// Gets the profile image
		/// </summary>
		/// <param name="userId"></param>
		/// <returns></returns>
		public FileResult GetImage(int userId)
		{
			FileResult toReturn = null;
			using (var context = new MatchingDatabase())
			{
				var currentUser = context.Users.FirstOrDefault(x => x.Id == userId);
				if (currentUser != null && currentUser.Image != null)
					toReturn = File(Server.MapPath(currentUser.Image), "image");
			}
			return toReturn ?? (File(Server.MapPath("~/App_Data/images/default.png"), "image/png"));
		}

		/// <summary>
		/// Finds the next matching person
		/// </summary>
		/// <param name="currentUserId">The user id of the current user</param>
		/// <returns></returns>
		public ActionResult GetPerson(int currentUserId)
		{
			var currentUser = new User {Id = currentUserId};

			var user = Dal.FindMatch(ref currentUser);
			if (user == null)
			//Todo redirect no match found
			return View("Index", currentUser);

			currentUser.Image = Url.Action("GetImage", "Home", new {userId = currentUser.Id});
			user.Image = Url.Action("GetImage", "Home", new {userId = user.Id});

			return View(new MatchingModel {CurrentUser = currentUser, MatchingUser = user});
		}

		// GET: Home
		/// <summary>
		/// The index page
		/// </summary>
		/// <returns></returns>
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult Match(int userId)
		{
			User currentUser = new User {Id = userId};
			User foundUser = Dal.MatchedUser(ref currentUser);

			currentUser.Image = Url.Action( "GetImage", "Home", new { userId = currentUser.Id } );
			foundUser.Image = Url.Action( "GetImage", "Home", new { userId = foundUser.Id } );
			return View( new MatchingModel
			{
				CurrentUser = currentUser,
				MatchingUser = foundUser
			} );
		}

	/// <summary>
		/// Attempts to match
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="otherUserId"></param>
		/// <param name="liked"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult Match(int userId, int otherUserId, bool liked)
		{
			ActionResult toReturn;
			var currentUser = new User {Id = userId};
			var likedUser = new User {Id = otherUserId};
			if ( Dal.LikePerson( ref currentUser, ref likedUser, liked ) )
			{
				toReturn =
					View(new MatchingModel
					     {
						     CurrentUser = currentUser,
						     MatchingUser = likedUser
					     });
			}
			else
				toReturn = RedirectToActionPermanent("GetPerson", new {currentUserId = userId});
			return toReturn;
		}
	}
}
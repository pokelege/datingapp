﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Matcher.Models
{
	public class RegistrationForm
	{
		[Display(Name = "BirthDate")]
		[Required(AllowEmptyStrings = false)]
		public DateTime BirthDate { get; set; }

		[Display(Name = "Gender")]
		[Required(AllowEmptyStrings = false)]
		public Gender Gender { get; set; }

		[Display(Name = "Profile picture")]
		public HttpPostedFileBase Image { get; set; }

		[Required(AllowEmptyStrings = false)]
		[Display(Name = "User Name")]
		public string UserName { get; set; }
	}
}
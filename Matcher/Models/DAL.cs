﻿using System;
using System.Diagnostics;
using System.Linq;

namespace Matcher.Models
{
	public class Dal
	{
		public static int AddUser(User user)
		{
			int toReturn;
			using (var context = new MatchingDatabase())
			{
				if (context.Users.Any(x => x.UserName == user.UserName))
					toReturn = -1;
				else
				{
				var currentUserObject = context.Users.Create();
				currentUserObject.UserName = user.UserName;
				currentUserObject.Birthdate = user.Birthdate;
				currentUserObject.IsMale = user.IsMale;
				currentUserObject.Image = user.Image;
				context.Users.Add(currentUserObject);
				context.SaveChanges();
				toReturn = currentUserObject.Id;
				}

			}
			return toReturn;
		}

		public static void FindUser(ref User user)
		{
			using ( var context = new MatchingDatabase() )
			{
				User toTest = user;
				if (context.Users.Any(x => x.UserName.Equals(toTest.UserName)))
				{
					user = context.Users.FirstOrDefault(x => x.UserName == toTest.UserName);
					user = new User{Birthdate = user.Birthdate, Id = user.Id, Image = user.Image, IsMale = user.IsMale, UserName = user.UserName};
				}
				else
				{
					user.Id = -1;
				}

			}
		}

		public static User FindMatch(ref User currentUser)
		{
			User toReturn = null;

			using (var context = new MatchingDatabase())
			{
				var userId = currentUser.Id;
				var currentUserTemp = context.Users.FirstOrDefault(x => x.Id == userId);
				//Todo add exception
				Debug.Assert(currentUserTemp != null, "currentUser != null");
				var users = context.Users
					.Where( x => x.Id != userId)
					.Where( x=> x.IsMale != currentUserTemp.IsMale)
					;
				var alreadyLiked = context.Matches
					.Where( x => x.UserId == currentUserTemp.Id )
					.Select( x => x.UserLike );
				if(alreadyLiked.Any())
				{
					users = users.Except( alreadyLiked );
				}
				if (users.Any())
				{
					toReturn = users.ToArray().ElementAt( new Random().Next( 0, users.Count() ) );
					toReturn = new User { Birthdate = toReturn.Birthdate, Id = toReturn.Id, Image = toReturn.Image, IsMale = toReturn.IsMale, UserName = toReturn.UserName };
				}

				currentUser = new User { Birthdate = currentUserTemp.Birthdate, Id = currentUserTemp.Id, Image = currentUserTemp.Image, IsMale = currentUserTemp.IsMale, UserName = currentUserTemp.UserName };
			}
			return toReturn;
		}

		public static bool LikePerson(ref User currentUser, ref User likedUser, bool liked)
		{
			bool toReturn;
			using (var context = new MatchingDatabase())
			{
				var match = context.Matches.Create();
				match.UserId = currentUser.Id;
				match.UserLikeId = likedUser.Id;
				match.Liked = liked;
				context.Matches.Add(match);
				context.SaveChanges();
				toReturn = (liked && context.Matches.Any(x => x.UserId == match.UserLikeId && x.UserLikeId == match.UserId && x.Liked));
			}
			return toReturn;
		}

		public static User MatchedUser(ref User currentUser)
		{
			User toReturn = null;
			using (var context = new MatchingDatabase())
			{
				int userId = currentUser.Id;
				currentUser = context.Users.FirstOrDefault(x => x.Id == userId);
				var otherLiked = context.Matches.Where(x => x.UserLikeId == userId && x.Liked).Select(x => x.User);
				var matches = context.Matches.Where( x => x.UserId == userId && x.Liked ).Select(x => x.UserLike);
				matches = matches.Intersect(otherLiked);
				if (matches.Any())
				{
					toReturn = matches.FirstOrDefault();
					if (toReturn != null)
						toReturn = new User { Birthdate = toReturn.Birthdate, Id = toReturn.Id, Image = toReturn.Image, IsMale = toReturn.IsMale, UserName = toReturn.UserName };
					else toReturn = new User();
				}
			}
			return toReturn;
		}
	}
}
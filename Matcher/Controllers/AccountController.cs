﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Matcher.Models;

namespace Matcher.Controllers
{
    public class AccountController : Controller
    {
	    /// <summary>
		/// The registration page
		/// </summary>
		/// <returns></returns>
		public ActionResult AddUser()
		{
			return View();
		}

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoginAuthentication(RegistrationForm form)
        {
            User loginUser  = new User();
            loginUser.UserName = form.UserName;
            Dal.FindUser(ref loginUser);
            if (loginUser.Id > 0)
            {
                return RedirectToActionPermanent("GetPerson", "Home", new { currentUserId = loginUser.Id });
            }
            else
            {
                return RedirectToActionPermanent("AddUser", "Account");
            }
        }

		/// <summary>
		/// Creates a user and immediately finds a match
		/// </summary>
		/// <param name="form"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult AddUser( RegistrationForm form )
		{
			string imageLocation = null;
			if ( form.Image != null && form.Image.ContentLength > 0 )
			{
				imageLocation = "~/App_Data/images/" + new Random().Next() + " - " + DateTime.Now.Ticks;
				if ( !Directory.Exists( Server.MapPath( "~/App_Data/images/" ) ) )
					Directory.CreateDirectory( Server.MapPath( "~/App_Data/images/" ) );
				form.Image.SaveAs( Server.MapPath( imageLocation ) );
			}

			int? currentUser = Dal.AddUser
				(
					new User
					{
						UserName = form.UserName,
						Birthdate = form.BirthDate,
						IsMale = form.Gender == Gender.Male,
						Image = imageLocation
					}
				);

			if (currentUser < 0)
			{
				ModelState.AddModelError( "UserName", "User exists" );
				return View(form);
			}
			return RedirectToActionPermanent( "GetPerson", "Home", new { currentUserId = currentUser.Value } );
		}
    }
}
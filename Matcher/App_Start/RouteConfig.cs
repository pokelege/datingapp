﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Matcher
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/",
                defaults: new { controller = "Account", action = "AddUser" }
            );
            routes.MapRoute(
                name: "GetPersonNew",
                url: "{controller}/{action}/{userName}",
                defaults: new { userName = "Unknown" }
            );
            routes.MapRoute(
                name: "GetPersonAgain",
                url: "{controller}/{action}/Find/{id}"
            );
        }
    }
}
